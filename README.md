# 上传文件到对象存储（OSS/COS）


#### 安装教程

```bash
composer require dgwht/file
```



#### 使用说明

> 初始化

```php
use dgwht/File;

//腾讯
//$FILE = new File([
//    "appid" => "AKIDzsbAXidT3iT6Ztl2oPVxxxxx",
//    "appkey" => "2s582nO1UYXMG6mK7qgcS8xxxxx",
//    "region" => "ap-chengdu",
//    "bucket" => "test-1252881831",
//],'Tx');

//阿里
$FILE = new File([
    "appid" => "LTAI4FdEgtr2kneRWgpSnzQB",
    "appkey" => "gSziEzn4t7bJg1jIpFGqeWYRdfwM55",
    "region" => "chengdu",
    "bucket" => "dgwht-test",
],'Ali');
```

> 上传

```php
$ret = $FILE->up("dd/1.php","index.php",[
    "Content-Disposition" => "attachment;filename=666.php"//阿里
    //"ContentDisposition" => "attachment;filename=666.php"//腾讯
]);
if($ret){
    echo "成功：[{$FILE->Msg}]";
}else{
    echo "失败：[{$FILE->Msg}]";
}
```

> 下载

```php
//获取签名后的下载地址
$url = $FILE->getSignUrl(15,"dd/1.php");
echo "<a href='{$url}'>{$url}</a>";
//header("Location: {$url}"); exit();
```

> 删除

```php
$ret = $FILE->del(["dd/1.php","dd/2.php","dd/3.php"]);
if($ret){
    echo "成功";
}else{
    echo "失败";
}
```

